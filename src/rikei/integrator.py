# coding=UTF-8
'''
Created on 2019/11/17

@author: agilecore co.,ltd.
'''

import sys
import pandas
import os.path
import xlrd
import csv
import logging
import sqlite3
from contextlib import closing
from rikei import definition_items as di

log_fmt = '%(levelname)s - %(message)s'
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

# 出力パス
dest = '../../LycheImpData'

# 不明リスト
UnknownList = []

def sa_convert(value, choice, alert=False):
    # 選択肢配列を作成
    選択肢 = []
    # 1要素目は選択肢名のため2要素目から繰り返し取得
    for i in range(1 ,len(choice)):
        選択肢.append(choice[i][0])

    try:
        # 合致した選択肢の番号をリターン
        return 選択肢.index(value) + 1
    except ValueError as e:
        # 見つからない場合は最大をリターンして不明リストへ追加
        if alert:
            raise e
        UnknownList.append([choice[0][0], value])
        return len(選択肢)

def get_sql_value(項目定義, ファイル情報):

    # 項目タイプ毎の変換
    if 項目定義[1][0] == 'SA':

        # SA変換モードで処理を分岐
        if 項目定義[5][0] == '選択肢変換':
            # 選択肢の番号に変換（前後の空白を取り除いて取得する）
            if str(ファイル情報[項目定義[3][0]-1]).strip() == '':
                setdata = ''
            else:
                setdata = sa_convert(str(ファイル情報[項目定義[3][0]-1]).strip(), 項目定義[6][0])

        elif 項目定義[5][0] == '0-2変換':
            # 0を2に変換
            if str(ファイル情報[項目定義[3][0]-1]) == '':
                setdata = ''
            else:
                if int(ファイル情報[項目定義[3][0]-1]) == 0:
                    setdata = 2
                elif int(ファイル情報[項目定義[3][0]-1]) == 1:
                    setdata = 1
                else:
                    logger.warn("0-2変換処理に「0」「1」以外のデータが存在します。")
                    setdata = int(ファイル情報[項目定義[3][0]-1])

        elif 項目定義[5][0] == '年度変換':
            # 「年」付与してSA変換を行う（例「2019」⇒「2019年」）
            if str(ファイル情報[項目定義[3][0]-1]) == '':
                setdata = ''
            else:
                setdata = sa_convert(str(ファイル情報[項目定義[3][0]-1]) + '年', 項目定義[6][0])

#                 # 半角⇒全角後に選択肢変換する
#                 text = str(ファイル情報[項目定義[3][0]-1]).translate(str.maketrans({chr(0x0021 + i): chr(0xFF01 + i) for i in range(94)}))
#                 setdata = sa_convert(text + '年', 項目定義[6][0])

#         # 基本的にこの変換はいらないか。。選択肢変換 or 変換なしでいけるはず
#         elif 項目定義[5][0] == '学年変換':
#             # 「年生」付与してSA変換を行う（例「3」⇒「3年生」）
#             if str(ファイル情報[項目定義[3][0]-1]) == '':
#                 setdata = ''
#             else:
#                 setdata = sa_convert(str(ファイル情報[項目定義[3][0]-1]) + '年生', 項目定義[6][0])

        elif 項目定義[5][0] == '':
            # 変換なし、そのまま取り込む
            if ファイル情報[項目定義[3][0]-1] == '':
                setdata = ''
            else:
                setdata = int(ファイル情報[項目定義[3][0]-1])
        else:
            logger.error("未対応のSA変換モードが存在します。変換モード：%s"  % (項目定義[5][0]))
            sys.exit()

    elif 項目定義[1][0] == 'MA':
        # 文字列初期化
        MA文字列 = ''
        比較文字列 = ''

        if 項目定義[5][0] == '0/1変換':

            # 取得列が複数でない場合
            if len(項目定義[3]) < 2:
                logger.error("項目タイプMAで取得列が複数でない定義が存在します。")
                sys.exit()

            # MA取得列の繰り返し
            for ma_col in 項目定義[3]:
                if ファイル情報[ma_col-1] == '':
                    # 空白が存在する場合はブランク扱い
                    MA文字列 = ''
                    break
                elif int(ファイル情報[ma_col-1]) in (0,1):
                    # 0,1を文字列として連結する
                    MA文字列 = MA文字列 + str(int(ファイル情報[ma_col-1]))
                else:
                    logger.error("項目タイプMAで0,1,空白以外のデータが存在します。")
                    sys.exit()

                比較文字列 = 比較文字列 + '0'

            # 全て'0'の場合「1」を付加、それ以外は「0」を付加
            if MA文字列 != '':
                if MA文字列 == 比較文字列:
                    MA文字列 = MA文字列 + '1'
                else:
                    MA文字列 = MA文字列 + '0'

            setdata = MA文字列

        elif 項目定義[5][0] == '':
            # 変換なし、そのまま取り込む
            if ファイル情報[項目定義[3][0]-1] == '':
                setdata = ''
            else:
                setdata = str(ファイル情報[項目定義[3][0]-1]).strip()
        else:
            logger.error("未対応のMA変換モードが存在します。変換モード：%s"  % (項目定義[5][0]))
            sys.exit()

    elif 項目定義[1][0] == '数値':

        # 整数、小数を判定して取得
        if 項目定義[6][3] == '':
            # 整数の場合
            if ファイル情報[項目定義[3][0]-1] == '':
                setdata = ''
            else:
                setdata = int(ファイル情報[項目定義[3][0]-1])
        else:
            # 小数の場合
            if ファイル情報[項目定義[3][0]-1] == '':
                setdata = ''
            else:
                setdata = ファイル情報[項目定義[3][0]-1]

    elif 項目定義[1][0] in ('文字列','自由回答'):
        # 文字列をそのまま取得
        setdata = ファイル情報[項目定義[3][0]-1]

    # sql設定値をリターン
    return setdata

def ベースデータ登録(conn, file, sd_row):
    logger.info("ベースデータファイル取込開始: %s" % file)
    c = conn.cursor()

    # 読み込みファイルを判定
    if os.path.splitext(file)[1] in ('.xls','.xlsx'):
#####ベースデータxlsxファイルの取込処理　ここから#####
        # 取込ファイルをオープン
        book = xlrd.open_workbook(file)
        # 1シート目を取得
        sheet = book.sheet_by_index(0)

        # データ分の繰り返し
        for f_row in range(1, sheet.nrows):

            # 行データをセット
            f_cols_value = sheet.row_values(f_row)

            # SQL文開始文字列
            sql = "insert into 統合 values ("

            # 取込みフラグをON
            imp_flg = 1
            sql_values = []

            # 項目定義リストを繰り返し
            for d_row in range(sd_row, len(di.項目定義リスト)):

                # ﾙｰﾌﾟ定義の場合スキップ
                if di.項目定義リスト[d_row][0][0] == 'Survey':
                    if di.項目定義リスト[d_row][1][0].startswith('ﾙｰﾌﾟ'):
                        continue

                # 取込みフラグがOFFの場合
                if imp_flg == 0:
                    # Surevyの場合は空を設定
                    if di.項目定義リスト[d_row][0][0] == 'Survey':
                        sql += ",?"
                        sql_values.append('')
                    continue

                # Tempの場合は空を設定
                if di.項目定義リスト[d_row][0][0] == 'Temp':
                    sql += ",?"
                    sql_values.append('')
                    continue

                # Makingの場合スキップ
                if di.項目定義リスト[d_row][0][0] == 'Making':
                    continue

                # 次の取込ファイルがきた場合、取込みフラグをOFF
                if di.項目定義リスト[d_row][0][0] == 'Impdata':
                    imp_flg = 0
                    continue

                # 初回か判定してSQL文を追加
                if sd_row == d_row:
                    sql += "?"
                else:
                    sql += ',?'

                # 取込みファイルの値をSQL文で取得
                sql_values.append(get_sql_value(di.項目定義リスト[d_row], f_cols_value))

            sql += ")"

            # Insert文発行
            try:
                c.execute(sql, (sql_values))
            except sqlite3.IntegrityError as e:
                logger.error('[%s] %s' % (sql_values[0], e))
                break;
#####ベースデータxlsxファイルの取込処理　ここまで#####
    elif os.path.splitext(file)[1] == '.csv':
#####ベースデータcsv ファイルの取込処理　ここから#####
        # 取込csvファイルをオープン
        with open(file, 'r', encoding='ms932') as fds:
            reader = csv.reader(fds)
            next(reader)

            # データ分の繰り返し
            for f_row in reader:

                # 行データをセット
                f_cols_value = f_row

                # SQL文開始文字列
                sql = "insert into 統合 values ("

                # 取込みフラグをON
                imp_flg = 1
                sql_values = []

                # 項目定義リストを繰り返し
                for d_row in range(sd_row, len(di.項目定義リスト)):

                    # ﾙｰﾌﾟ定義の場合スキップ
                    if di.項目定義リスト[d_row][0][0] == 'Survey':
                        if di.項目定義リスト[d_row][1][0].startswith('ﾙｰﾌﾟ'):
                            continue

                    # 取込みフラグがOFFの場合
                    if imp_flg == 0:
                        # Surevyの場合は空を設定
                        if di.項目定義リスト[d_row][0][0] == 'Survey':
                            sql += ",?"
                            sql_values.append('')
                        continue

                    # Tempの場合は空を設定
                    if di.項目定義リスト[d_row][0][0] == 'Temp':
                        sql += ",?"
                        sql_values.append('')
                        continue

                    # Makingの場合スキップ
                    if di.項目定義リスト[d_row][0][0] == 'Making':
                        continue

                    # 次の取込ファイルがきた場合、取込みフラグをOFF
                    if di.項目定義リスト[d_row][0][0] == 'Impdata':
                        imp_flg = 0
                        continue

                    # 初回か判定してSQL文を追加
                    if sd_row == d_row:
                        sql += "?"
                    else:
                        sql += ',?'

                    # 取込みファイルの値をSQL文で取得
                    sql_values.append(get_sql_value(di.項目定義リスト[d_row], f_cols_value))

                sql += ")"

                # Insert文発行
                try:
                    c.execute(sql, (sql_values))
                except sqlite3.IntegrityError as e:
                    logger.error('[%s] %s' % (sql_values[0], e))
                    break;
#####ベースデータcsv ファイルの取込処理　ここまで#####
    else:
        logger.error("未対応のファイル拡張子です。" )
        sys.exit()

    conn.commit()

    logger.info("ベースデータファイル取込終了")

def 更新データ登録(conn, file, sd_row):
    logger.info("更新データファイル取込開始: %s" % file)

    c = conn.cursor()

    sql_serch = "select count(1) from 統合"
    sql_where = ""

    # 検索条件SQLを作成
    初回フラグ = 1
    for d_row in range(sd_row, len(di.項目定義リスト)):
        if di.項目定義リスト[d_row][0][0] != 'Joken':
            break
        if di.項目定義リスト[d_row][4][0] == '':
            continue
        if 初回フラグ == 1:
            sql_where += " where %s = ?" % di.項目定義リスト[d_row][4][0]
            初回フラグ = 0
        else:
            sql_where += " and %s = ?" % di.項目定義リスト[d_row][4][0]

    # 読み込みファイルを判定
    if os.path.splitext(file)[1] in ('.xls','.xlsx'):
#####更新データxlsxファイルの取込処理　ここから#####
        # 取込ファイルをオープン
        book = xlrd.open_workbook(file)
        # 1シート目を取得
        sheet = book.sheet_by_index(0)

        # データ分の繰り返し
        for f_row in range(1, sheet.nrows):

            # ファイルから1行データを取得
            f_cols_value = sheet.row_values(f_row)

            # 検索条件値を取得
            serch_key = []
            for d_row in range(sd_row, len(di.項目定義リスト)):
                if di.項目定義リスト[d_row][0][0] != 'Joken':
                    break
                if di.項目定義リスト[d_row][4][0] == '':
                    continue
                serch_key.append(get_sql_value(di.項目定義リスト[d_row], f_cols_value))

            # 統合テーブルから検索
            c.execute(sql_serch + sql_where, (serch_key))
            rs = c.fetchone()

            if rs[0] == 0:
                # 統合テーブルに存在しない場合、ログ出力
                logger.warning('統合データに存在しません。%s' % serch_key)
            elif rs[0] == 1:
                # 統合テーブルに存在する場合、Update
                sql = "update 統合 set "

                初回フラグ = 1
                sql_values = []

                # 項目定義リストを繰り返し
                for d_row in range(sd_row, len(di.項目定義リスト)):

                    # 次の取込ファイルがきた場合、項目ループを抜けてコミットへ
                    if di.項目定義リスト[d_row][0][0] == 'Impdata':
                        break

                    # ﾙｰﾌﾟ定義の場合スキップ
                    if di.項目定義リスト[d_row][0][0] == 'Survey':
                        if di.項目定義リスト[d_row][1][0].startswith('ﾙｰﾌﾟ'):
                            continue

                    # Joken、Makingの場合スキップ
                    if di.項目定義リスト[d_row][0][0] in ('Joken','Making'):
                        continue

                    if di.項目定義リスト[d_row][0][0] == 'Merge':
                        # 既存項目への更新がある場合、対象カラムIDを取得
                        if di.項目定義リスト[d_row][4][0] != '':
                            target_col = di.項目定義リスト[d_row][4][0]
                        else:
                            continue

                        # 更新モードの場合Tempは存在しない
                    if di.項目定義リスト[d_row][0][0] == 'Temp':
                        logger.error('更新モードでTemp項目が存在します。')
                        sys.exit

                    if di.項目定義リスト[d_row][0][0] == 'Survey':
                        # 更新対象カラムIDを取得
                        target_col = di.項目定義リスト[d_row][2][0]

                    # 初回か判定してSQL文を追加
                    if 初回フラグ == 1:
                        sql += "%s = ?" % (target_col)
                        初回フラグ = 0
                    else:
                        sql += ',%s = ?' % (target_col)

                    # 取込みファイルの値をSQL文で取得
                    sql_values.append(get_sql_value(di.項目定義リスト[d_row], f_cols_value))

                # 検索条件文を追加
                sql += sql_where

                # 更新条件を追加
                for i in serch_key:
                    sql_values.append(i)

                # Update文発行
                try:
                    c.execute(sql, (sql_values))
                except sqlite3.IntegrityError as e:
                    logger.error('[%s] %s' % (sql_values[0], e))
                    break;
            else:
                # 取得結果が複数は検索が不正
                logger.error('（複数取得）更新データのカラム情報が不正です。%s' % serch_key)
                sys.exit()
#####更新データxlsxファイルの取込処理　ここまで#####
    elif os.path.splitext(file)[1] == '.csv':
#####更新データcsv ファイルの取込処理　ここから#####
        # 取込ファイルをオープン
        with open(file, 'r', encoding='ms932') as fds:
            reader = csv.reader(fds)
            next(reader)

            # データ分の繰り返し
            for f_row in reader:

                # 行データをセット
                f_cols_value = f_row

                # 検索条件値を取得
                serch_key = []
                for d_row in range(sd_row, len(di.項目定義リスト)):
                    if di.項目定義リスト[d_row][0][0] != 'Joken':
                        break
                    if di.項目定義リスト[d_row][4][0] == '':
                        continue
                    serch_key.append(get_sql_value(di.項目定義リスト[d_row], f_cols_value))

                # 統合テーブルから検索
                c.execute(sql_serch + sql_where, (serch_key))
                rs = c.fetchone()

                if rs[0] == 0:
                    # 統合テーブルに存在しない場合、ログ出力
                    logger.warning('統合データに存在しません。%s' % serch_key)
                elif rs[0] == 1:
                    # 統合テーブルに存在する場合、Update
                    sql = "update 統合 set "

                    初回フラグ = 1
                    sql_values = []

                    # 項目定義リストを繰り返し
                    for d_row in range(sd_row, len(di.項目定義リスト)):

                        # 次の取込ファイルがきた場合、項目ループを抜けてコミットへ
                        if di.項目定義リスト[d_row][0][0] == 'Impdata':
                            break

                        # ﾙｰﾌﾟ定義の場合スキップ
                        if di.項目定義リスト[d_row][0][0] == 'Survey':
                            if di.項目定義リスト[d_row][1][0].startswith('ﾙｰﾌﾟ'):
                                continue

                        # Making、Tempの場合スキップ
                        if di.項目定義リスト[d_row][0][0] in ('Joken','Making'):
                            continue

                        if di.項目定義リスト[d_row][0][0] == 'Merge':
                            # 既存項目への更新がある場合、対象カラムIDを取得
                            if di.項目定義リスト[d_row][4][0] != '':
                                target_col = di.項目定義リスト[d_row][4][0]
                            else:
                                continue

                            # 更新モードの場合Tempは存在しない
                        if di.項目定義リスト[d_row][0][0] == 'Temp':
                            logger.error('更新モードでTemp項目が存在します。')
                            sys.exit

                        if di.項目定義リスト[d_row][0][0] == 'Survey':
                            # 更新対象カラムIDを取得
                            target_col = di.項目定義リスト[d_row][2][0]

                        # 初回か判定してSQL文を追加
                        if 初回フラグ == 1:
                            sql += "%s = ?" % (target_col)
                            初回フラグ = 0
                        else:
                            sql += ',%s = ?' % (target_col)

                        # 取込みファイルの値をSQL文で取得
                        sql_values.append(get_sql_value(di.項目定義リスト[d_row], f_cols_value))

                    # 検索条件文を追加
                    sql += sql_where

                    # 更新条件を追加
                    for i in serch_key:
                        sql_values.append(i)

                    # Update文発行
                    try:
                        c.execute(sql, (sql_values))
                    except sqlite3.IntegrityError as e:
                        logger.error('[%s] %s' % (sql_values[0], e))
                        break;
                else:
                    # 取得結果が複数は検索が不正
                    logger.error('（複数取得）更新データのカラム情報が不正です。%s' % serch_key)
                    sys.exit()
#####更新データcsv ファイルの取込処理　ここまで#####
    else:
        logger.error("未対応のファイル拡張子です。" )
        sys.exit()

    conn.commit()

    logger.info("更新データファイル取込終了")

def 結合データ登録(conn, file, sd_row):
    logger.info("結合データファイル取込開始: %s" % file)

    c = conn.cursor()

    sql_serch = "select count(1) from 統合"
    sql_where = ""

    # 検索条件SQLを作成
    初回フラグ = 1
    for d_row in range(sd_row, len(di.項目定義リスト)):
        if di.項目定義リスト[d_row][0][0] != 'Joken':
            break
        if di.項目定義リスト[d_row][4][0] == '':
            continue
        if 初回フラグ == 1:
            sql_where += " where %s = ?" % di.項目定義リスト[d_row][4][0]
            初回フラグ = 0
        else:
            sql_where += " and %s = ?" % di.項目定義リスト[d_row][4][0]

    # 読み込みファイルを判定
    if os.path.splitext(file)[1] in ('.xls','.xlsx'):
#####結合データxlsxファイルの取込処理　ここから#####
        # 取込ファイルをオープン
        book = xlrd.open_workbook(file)
        # 1シート目を取得
        sheet = book.sheet_by_index(0)

        # データ分の繰り返し
        for f_row in range(1, sheet.nrows):

            # ファイルから1行データを取得
            f_cols_value = sheet.row_values(f_row)

            # 検索条件値を取得
            serch_key = []
            for d_row in range(sd_row, len(di.項目定義リスト)):
                if di.項目定義リスト[d_row][0][0] != 'Joken':
                    break
                if di.項目定義リスト[d_row][4][0] == '':
                    continue
                serch_key.append(get_sql_value(di.項目定義リスト[d_row], f_cols_value))

            # 統合テーブルから検索
            c.execute(sql_serch + sql_where, (serch_key))
            rs = c.fetchone()

            if rs[0] == 0:
                # 統合テーブルに存在しない場合、Insert
                sql = "insert into 統合 values ("

                # 取込みフラグをON
                imp_flg = 1

                初回フラグ = 1
                sql_values = []

                # 項目定義リストを繰り返し
                for d_row in range(sd_row, len(di.項目定義リスト)):

                    # ﾙｰﾌﾟ定義の場合スキップ
                    if di.項目定義リスト[d_row][0][0] == 'Survey':
                        if di.項目定義リスト[d_row][1][0].startswith('ﾙｰﾌﾟ'):
                            continue

                    # 取込みフラグがOFFの場合
                    if imp_flg == 0:
                        # Surevyの場合は空を設定
                        if di.項目定義リスト[d_row][0][0] == 'Survey':
                            sql += ",?"
                            sql_values.append('')
                        continue

                    # Tempの場合は空を設定
                    if di.項目定義リスト[d_row][0][0] == 'Temp':
                        sql += ",?"
                        sql_values.append('')
                        continue

                    # Makingの場合スキップ
                    if di.項目定義リスト[d_row][0][0] == 'Making':
                        continue

                    # 次の取込ファイルがきた場合、取込みフラグをOFF
                    if di.項目定義リスト[d_row][0][0] == 'Impdata':
                        imp_flg = 0
                        continue

                    # 初回か判定してSQL文を追加
                    if 初回フラグ == 1:
                        sql += "?"
                        初回フラグ = 0
                    else:
                        sql += ',?'

                    # 取込みファイルの値をSQL文で取得
                    sql_values.append(get_sql_value(di.項目定義リスト[d_row], f_cols_value))

                sql += ")"

                # Insert文発行
                try:
                    c.execute(sql, (sql_values))
                except sqlite3.IntegrityError as e:
                    logger.error('[%s] %s' % (sql_values[0], e))
                    break;
            elif rs[0] == 1:
                # 統合テーブルに存在する場合、Update
                sql = "update 統合 set "

                初回フラグ = 1
                sql_values = []

                # 項目定義リストを繰り返し
                for d_row in range(sd_row, len(di.項目定義リスト)):

                    # 次の取込ファイルがきた場合、項目ループを抜けてコミットへ
                    if di.項目定義リスト[d_row][0][0] == 'Impdata':
                        break

                    # ﾙｰﾌﾟ定義の場合スキップ
                    if di.項目定義リスト[d_row][0][0] == 'Survey':
                        if di.項目定義リスト[d_row][1][0].startswith('ﾙｰﾌﾟ'):
                            continue

                    # Joken、Making、Tempの場合スキップ
                    if di.項目定義リスト[d_row][0][0] in ('Joken','Making','Temp'):
                        continue

                    if di.項目定義リスト[d_row][0][0] == 'Merge':
                        # 既存項目への更新がある場合、対象カラムIDを取得
                        if di.項目定義リスト[d_row][4][0] != '':
                            target_col = di.項目定義リスト[d_row][4][0]
                        else:
                            continue

                    if di.項目定義リスト[d_row][0][0] == 'Survey':
                        # 更新対象カラムIDを取得
                        target_col = di.項目定義リスト[d_row][2][0]

                    # 初回か判定してSQL文を追加
                    if 初回フラグ == 1:
                        sql += "%s = ?" % (target_col)
                        初回フラグ = 0
                    else:
                        sql += ',%s = ?' % (target_col)

                    # 取込みファイルの値をSQL文で取得
                    sql_values.append(get_sql_value(di.項目定義リスト[d_row], f_cols_value))

                # 検索条件文を追加
                sql += sql_where

                # 更新条件を追加
                for i in serch_key:
                    sql_values.append(i)

                # Update文発行
                try:
                    c.execute(sql, (sql_values))
                except sqlite3.IntegrityError as e:
                    logger.error('[%s] %s' % (sql_values[0], e))
                    break;
            else:
                # 取得結果が複数は検索が不正
                logger.error('（複数取得）結合データのカラム情報が不正です。%s' % serch_key)
                sys.exit()
#####結合データxlsxファイルの取込処理　ここまで#####
    elif os.path.splitext(file)[1] == '.csv':
#####結合データcsv ファイルの取込処理　ここから#####
        # 取込ファイルをオープン
        with open(file, 'r', encoding='ms932') as fds:
            reader = csv.reader(fds)
            next(reader)

            # データ分の繰り返し
            for f_row in reader:

                # 行データをセット
                f_cols_value = f_row

                # 検索条件値を取得
                serch_key = []
                for d_row in range(sd_row, len(di.項目定義リスト)):
                    if di.項目定義リスト[d_row][0][0] != 'Joken':
                        break
                    if di.項目定義リスト[d_row][4][0] == '':
                        continue
                    serch_key.append(get_sql_value(di.項目定義リスト[d_row], f_cols_value))

                # 統合テーブルから検索
                c.execute(sql_serch + sql_where, (serch_key))
                rs = c.fetchone()

                if rs[0] == 0:
                    # 統合テーブルに存在しない場合、Insert
                    sql = "insert into 統合 values ("

                    # 取込みフラグをON
                    imp_flg = 1

                    初回フラグ = 1
                    sql_values = []

                    # 項目定義リストを繰り返し
                    for d_row in range(sd_row, len(di.項目定義リスト)):

                        # ﾙｰﾌﾟ定義の場合スキップ
                        if di.項目定義リスト[d_row][0][0] == 'Survey':
                            if di.項目定義リスト[d_row][1][0].startswith('ﾙｰﾌﾟ'):
                                continue

                        # 取込みフラグがOFFの場合
                        if imp_flg == 0:
                            # Surevyの場合は空を設定
                            if di.項目定義リスト[d_row][0][0] == 'Survey':
                                sql += ",?"
                                sql_values.append('')
                            continue

                        # Tempの場合は空を設定
                        if di.項目定義リスト[d_row][0][0] == 'Temp':
                            sql += ",?"
                            sql_values.append('')
                            continue

                        # Makingの場合スキップ
                        if di.項目定義リスト[d_row][0][0] == 'Making':
                            continue

                        # 次の取込ファイルがきた場合、取込みフラグをOFF
                        if di.項目定義リスト[d_row][0][0] == 'Impdata':
                            imp_flg = 0
                            continue

                        # 初回か判定してSQL文を追加
                        if 初回フラグ == 1:
                            sql += "?"
                            初回フラグ = 0
                        else:
                            sql += ',?'

                        # 取込みファイルの値をSQL文で取得
                        sql_values.append(get_sql_value(di.項目定義リスト[d_row], f_cols_value))

                    sql += ")"

                    # Insert文発行
                    try:
                        c.execute(sql, (sql_values))
                    except sqlite3.IntegrityError as e:
                        logger.error('[%s] %s' % (sql_values[0], e))
                        break;
                elif rs[0] == 1:
                    # 統合テーブルに存在する場合、Update
                    sql = "update 統合 set "

                    初回フラグ = 1
                    sql_values = []

                    # 項目定義リストを繰り返し
                    for d_row in range(sd_row, len(di.項目定義リスト)):

                        # 次の取込ファイルがきた場合、項目ループを抜けてコミットへ
                        if di.項目定義リスト[d_row][0][0] == 'Impdata':
                            break

                        # ﾙｰﾌﾟ定義の場合スキップ
                        if di.項目定義リスト[d_row][0][0] == 'Survey':
                            if di.項目定義リスト[d_row][1][0].startswith('ﾙｰﾌﾟ'):
                                continue

                        # Joken、Making、Tempの場合スキップ
                        if di.項目定義リスト[d_row][0][0] in ('Joken','Making','Temp'):
                            continue

                        if di.項目定義リスト[d_row][0][0] == 'Merge':
                            # 既存項目への更新がある場合、対象カラムIDを取得
                            if di.項目定義リスト[d_row][4][0] != '':
                                target_col = di.項目定義リスト[d_row][4][0]
                            else:
                                continue

                        if di.項目定義リスト[d_row][0][0] == 'Survey':
                            # 更新対象カラムIDを取得
                            target_col = di.項目定義リスト[d_row][2][0]

                        # 初回か判定してSQL文を追加
                        if 初回フラグ == 1:
                            sql += "%s = ?" % (target_col)
                            初回フラグ = 0
                        else:
                            sql += ',%s = ?' % (target_col)

                        # 取込みファイルの値をSQL文で取得
                        sql_values.append(get_sql_value(di.項目定義リスト[d_row], f_cols_value))

                    # 検索条件文を追加
                    sql += sql_where

                    # 更新条件を追加
                    for i in serch_key:
                        sql_values.append(i)

                    # Update文発行
                    try:
                        c.execute(sql, (sql_values))
                    except sqlite3.IntegrityError as e:
                        logger.error('[%s] %s' % (sql_values[0], e))
                        break;
                else:
                    # 取得結果が複数は検索が不正
                    logger.error('（複数取得）結合データのカラム情報が不正です。%s' % serch_key)
                    sys.exit()
#####結合データcsv ファイルの取込処理　ここまで#####
    else:
        logger.error("未対応のファイル拡張子です。" )
        sys.exit()

    conn.commit()

    logger.info("結合データファイル取込終了")

def init_table(conn):
    c = conn.cursor()

    logger.info("Lyche登録用メモリテーブル作成開始:" )

    sql = "create table 統合 ("
    初回フラグ = 1

    # 定義リストの要素分を繰り返してCreate文を作成する。
    for 項目定義 in di.項目定義リスト:

        # 取込み情報、及びMakingの場合スキップ
        if 項目定義[0][0] in ('Impdata','Merge','Making','Temp'):
            continue

        # ﾙｰﾌﾟ定義の場合スキップ
        if 項目定義[0][0] == 'Survey':
            if 項目定義[1][0].startswith('ﾙｰﾌﾟ'):
                continue

        # 検索条件でカラム情報がない場合スキップ
        if 項目定義[0][0] == 'Joken':
            if 項目定義[2][0] == '':
                continue

        if 初回フラグ == 1:

            # 項目タイプ毎でカラムを設定する
            if 項目定義[1][0] in ('SA','数値'):
                # 数値カラムを追加
                if 項目定義[4][0] == 'PK':
                    sql += "%s integer primary key" % (項目定義[2][0])
                else:
                    sql += "%s integer" % (項目定義[2][0])

            elif 項目定義[1][0] in ('MA','文字列','自由回答'):
                # テキストカラムを追加
                if 項目定義[4][0] == 'PK':
                    sql += "%s text primary key" % (項目定義[2][0])
                else:
                    sql += "%s text" % (項目定義[2][0])

            else:
                logger.error("未対応の項目タイプが存在します。項目タイプ：%s"  % (項目定義[1][0]))
                sys.exit()

            初回フラグ = 0
        else:

            # 項目タイプ毎でカラムを設定する
            if 項目定義[1][0] in ('SA','数値'):
                # 数値カラムを追加
                if 項目定義[4][0] == 'PK':
                    sql += " ,%s integer primary key" % (項目定義[2][0])
                else:
                    sql += " ,%s integer" % (項目定義[2][0])

            elif 項目定義[1][0] in ('MA','文字列','自由回答'):
                # テキストカラムを追加
                if 項目定義[4][0] == 'PK':
                    sql += " ,%s text primary key" % (項目定義[2][0])
                else:
                    sql += " ,%s text" % (項目定義[2][0])

            else:
                logger.error("未対応の項目タイプが存在します。項目タイプ：%s"  % (項目定義[1][0]))
                sys.exit()

    sql += ')'

    # 空のテーブル作成
    c.execute(sql)

    sql = "CREATE UNIQUE INDEX ユニークインデックス ON 統合 ("
    初回フラグ = 1

    # 定義リストの要素分を繰り返してCreate文を作成する。
    for 項目定義 in di.項目定義リスト:

        # 取込み情報の場合、次へ
        if 項目定義[0][0] == 'Impdata':
            continue

        # Joken以外が来た場合終了
        if 項目定義[0][0] in ('Survey','Merge','Making','Temp'):
            break

        if 初回フラグ == 1:
            if 項目定義[4][0] == 'UK':
                sql += "%s" % (項目定義[2][0])
                初回フラグ = 0
        else:
            if 項目定義[4][0] == 'UK':
                sql += " ,%s" % (項目定義[2][0])

    sql += ')'

    # ユニークインデックス作成
    c.execute(sql)

    logger.info("Lyche登録用メモリテーブル作成終了:" )

def imp_data(conn):
    logger.info("データ取り込み開始")

    # 定義リストの要素分を繰り返す
    for 行番号, 項目定義 in enumerate(di.項目定義リスト):

        # 'Impdata'の要素を検索する
        if 項目定義[0][0] in ('Joken','Survey','Making','Merge','Temp'):
            # 'Impdata'でない場合次へ
            continue

        # 定義出力でエラーでなければImpdataで決定
        if 項目定義[2][0] == 'ベース':
            # ベース（ライチ出力データや学籍情報など）※INSERTを行う
            ベースデータ登録(conn, 項目定義[1][0], 行番号 + 1)

        elif 項目定義[2][0] == '更新':
            # 更新（プレイスメントやアンケートなど）※UPDATEを行う
            更新データ登録(conn, 項目定義[1][0], 行番号 + 1)

        elif 項目定義[2][0] == '結合':
            # 結合（追加受領データなど）※ベースに存在する場合はUPDATE、存在しない場合INSERTを行う
            結合データ登録(conn, 項目定義[1][0], 行番号 + 1)

        else:
            logger.error("未対応の取込モードが存在します。：%s"  % (項目定義[2][0]))
            sys.exit()

    logger.info("データ取り込み終了")

def generate_import_csv(conn, file):
    logger.info("インポートデータファイル作成開始: %s" % file)

    c = conn.cursor()

    sql = "select "
    初回フラグ = 1

    # 定義リストの要素分を繰り返してSelect文を作成する。
    for 項目定義 in di.項目定義リスト:

        # 取込み情報、及びMakingの場合スキップ
        if 項目定義[0][0] in ('Impdata','Joken','Merge','Making','Temp'):
            continue

        # ﾙｰﾌﾟ定義の場合スキップ
        if 項目定義[0][0] == 'Survey':
            if 項目定義[1][0].startswith('ﾙｰﾌﾟ'):
                continue

        if 初回フラグ == 1:

            sql += "%s" % (項目定義[2][0])
            初回フラグ = 0
        else:

            sql += " ,%s" % (項目定義[2][0])

    sql += " from 統合"

    c.execute(sql)

    rs = c.fetchall()

    # ヘッダー取得
    hedder = []
    for rh in c.description:
        hedder.append(rh[0])

    with open(file, 'w', encoding='utf_8_sig', newline="") as fdw:
        writer = csv.writer(fdw, lineterminator='\r\n')

        # ヘッダー設定
        writer.writerow(hedder)

        # 結果をCSV出力
        for row in rs:
            writer.writerow(row)

    logger.info("インポートデータファイル作成終了")

def generate_import_xlsx(conn, file):
    logger.info("インポートデータファイル作成開始: %s" % file)

    c = conn.cursor()

    sql = "select "
    初回フラグ = 1

    # 定義リストの要素分を繰り返してSelect文を作成する。
    for 項目定義 in di.項目定義リスト:

        # 取込み情報、及びMakingの場合スキップ
        if 項目定義[0][0] in ('Impdata','Joken','Merge','Making','Temp'):
            continue

        # ﾙｰﾌﾟ定義の場合スキップ
        if 項目定義[0][0] == 'Survey':
            if 項目定義[1][0].startswith('ﾙｰﾌﾟ'):
                continue

        if 初回フラグ == 1:

            sql += "%s" % (項目定義[2][0])
            初回フラグ = 0
        else:

            sql += " ,%s" % (項目定義[2][0])

    sql += " from 統合"

    c.execute(sql)

    ITEM = []

    # ヘッダー取得
    hedder = []
    for rh in c.description:
        hedder.append(rh[0])
    ITEM.append(hedder)

    # SELECT結果取得
    rs = c.fetchall()
    for rb in rs:
        ITEM.append(rb)

    # 結果をXLSX出力
    df = pandas.DataFrame(ITEM)
    df.to_excel(file, index=False, header=False)

    logger.info("インポートデータファイル作成終了")

# # ライチはcsvで定義取り込めないため、xlsxの一択（復活する際はgenerate_definition_xlsxを引用して最新化すること）
# def generate_definition_csv(file):
#     logger.info("項目定義ファイル作成開始: %s" % file)
#
#     # csvファイルをオープンする
#     with open(file, 'w', encoding='utf_8_sig', newline="") as fdw:
#         writer = csv.writer(fdw, lineterminator='\r\n')
#
#         # 定義ファイルヘッダーを設定する
#         writer.writerow(['', '', '項目', '項目', '客先', 'ループ', '', '', '選択肢', '選択肢', '桁数', '最小値', '最大値', '小数点', '', 'カテゴライズ', '特別条件', '集計ベース', '', '調査票'])
#         writer.writerow(['行区分', '形式', 'コード', 'タイプ', '表示', '子項目', 'ID', 'ラベル', 'ﾀｲﾌﾟ', 'ｳｴｲﾄ', '', '', '', '以下桁', '統計量', '条件', '', '集計ベースタイプ', '条件', 'ラベル'])
#
#         MakingPlace = []
#         ループフラグ = 0
#
#         # 定義リストの要素分を繰り返す
#         for d_row in range(0, len(di.項目定義リスト)):
#
#             # 定義に関係ない項目はスキップ
#             if di.項目定義リスト[d_row][0][0] in ('Impdata','Joken','Temp','Merge'):
#                 continue
#
#             # 形式、項目タイプ毎で項目定義を設定する
#             if di.項目定義リスト[d_row][0][0] == 'Survey' and di.項目定義リスト[d_row][1][0] in ('SA','MA'):
#
#                 if ループフラグ == 0:
#                     writer.writerow(['項目', di.項目定義リスト[d_row][0][0], '', di.項目定義リスト[d_row][1][0], '○', '', di.項目定義リスト[d_row][2][0], di.項目定義リスト[d_row][7][0], '', '', '', '', '', '', '', '', '', '', '', '', ])
#                     # 選択肢リストの要素分を繰り返す
#                     for i in range(1, len(di.項目定義リスト[d_row][6][0])):
#                         writer.writerow(['', '', '', '', '', '', i, di.項目定義リスト[d_row][6][0][i][0], di.項目定義リスト[d_row][6][0][i][1], '', '', '', '', '', '', '', '', '', '', '', ])
#                 else:
#                     writer.writerow(['', '', '', '', '○', '子項目', di.項目定義リスト[d_row][2][0], di.項目定義リスト[d_row][7][0], '', '', '', '', '', '', '', '', '', '', '', '', ])
#                     # 次の要素が子項目でない場合
#                     if di.項目定義リスト[d_row + 1][0][0] != 'Impdata':
#                         if di.項目定義リスト[d_row + 1][8][0] != '〇':
#                             # 選択肢リストの要素分を繰り返す
#                             for i in range(1, len(di.項目定義リスト[d_row][6][0])):
#                                 writer.writerow(['', '', '', '', '', '', i, di.項目定義リスト[d_row][6][0][i][0], di.項目定義リスト[d_row][6][0][i][1], '', '', '', '', '', '', '', '', '', '', '', ])
#                             ループフラグ = 0
#                     else:
#                         ループフラグ = 0
#
#             elif di.項目定義リスト[d_row][0][0] == 'Survey' and di.項目定義リスト[d_row][1][0] in ('数値','文字列'):
#
#                 if ループフラグ == 0:
#                     writer.writerow(['項目', di.項目定義リスト[d_row][0][0], '', di.項目定義リスト[d_row][1][0], '○', '', di.項目定義リスト[d_row][2][0], di.項目定義リスト[d_row][7][0], '', '', di.項目定義リスト[d_row][6][0], di.項目定義リスト[d_row][6][1], di.項目定義リスト[d_row][6][2],  di.項目定義リスト[d_row][6][3], '', '', '', '', '', '', ])
#                     writer.writerow(['', '', '', '', '', '', '*', '不明', '不明', '', '', '', '', '', '', '', '', '', '', '', ])
#                 else:
#                     writer.writerow(['', '', '', '', '○', '子項目', di.項目定義リスト[d_row][2][0], di.項目定義リスト[d_row][7][0], '', '', di.項目定義リスト[d_row][6][0], di.項目定義リスト[d_row][6][1], di.項目定義リスト[d_row][6][2],  di.項目定義リスト[d_row][6][3], '', '', '', '', '', '', ])
#                     # 次の要素が子項目でない場合
#                     if di.項目定義リスト[d_row + 1][8][0] != '〇':
#                         # 次の要素が子項目でない場合、不明を追加してフラグOFF
#                         writer.writerow(['', '', '', '', '', '', '*', '不明', '不明', '', '', '', '', '', '', '', '', '', '', '', ])
#                         ループフラグ = 0
#
#             elif di.項目定義リスト[d_row][0][0] == 'Survey' and di.項目定義リスト[d_row][1][0] == '自由回答':
#
#                 if ループフラグ == 0:
#                     writer.writerow(['項目', di.項目定義リスト[d_row][0][0], '', di.項目定義リスト[d_row][1][0], '○', '', di.項目定義リスト[d_row][2][0], di.項目定義リスト[d_row][7][0], '', '', '', '', '', '', '', '', '', '', '', '', ])
#                     writer.writerow(['', '', '', '', '', '', '*', '不明', '不明', '', '', '', '', '', '', '', '', '', '', '', ])
#                 else:
#                     writer.writerow(['', '', '', '', '○', '子項目', di.項目定義リスト[d_row][2][0], di.項目定義リスト[d_row][7][0], '', '', '', '', '', '', '', '', '', '', '', '', ])
#                     # 次の要素が子項目でない場合
#                     if di.項目定義リスト[d_row + 1][8][0] != '〇':
#                         # 次の要素が子項目でない場合、不明を追加してフラグOFF
#                         writer.writerow(['', '', '', '', '', '', '*', '不明', '不明', '', '', '', '', '', '', '', '', '', '', '', ])
#                         ループフラグ = 0
#
#             elif di.項目定義リスト[d_row][0][0] == 'Making' and di.項目定義リスト[d_row][1][0] in ('SA','MA'):
#
#                 if ループフラグ == 0:
#
#                     MakingPlace.append(di.項目定義リスト[d_row][2][0])
#
#                     writer.writerow(['項目', di.項目定義リスト[d_row][0][0], '', di.項目定義リスト[d_row][1][0], '○', '', di.項目定義リスト[d_row][2][0], di.項目定義リスト[d_row][7][0], '', '', '', '', '', '', '', '', '', '', '', '', ])
#                     # 選択肢リストの要素分を繰り返す
#                     for i in range(1, len(di.項目定義リスト[d_row][6][0])):
#                         writer.writerow(['', '', '', '', '', '', i, di.項目定義リスト[d_row][6][0][i][0], di.項目定義リスト[d_row][6][0][i][1], '', '', '', '', '', '', '条件指定', '', '', '', '', ])
#                 else:
#                     writer.writerow(['', '', '', '', '○', '子項目', di.項目定義リスト[d_row][2][0], di.項目定義リスト[d_row][7][0], '', '', '', '', '', '', '', '', '', '', '', '', ])
#                     # 次の要素が子項目でない場合
#                     if di.項目定義リスト[d_row + 1][0][0] != 'Impdata':
#                         if di.項目定義リスト[d_row + 1][8][0] != '〇':
#                             # 選択肢リストの要素分を繰り返す
#                             for i in range(1, len(di.項目定義リスト[d_row][6][0])):
#                                 writer.writerow(['', '', '', '', '', '', i, di.項目定義リスト[d_row][6][0][i][0], di.項目定義リスト[d_row][6][0][i][1], '', '', '', '', '', '', '条件指定', '', '', '', '', ])
#                             ループフラグ = 0
#                     else:
#                         ループフラグ = 0
#
# #Makingでこのタイプはないか
#             elif di.項目定義リスト[d_row][0][0] == 'Making' and di.項目定義リスト[d_row][1][0] in ('数値','文字列'):
#
#                 if ループフラグ == 0:
#
#                     MakingPlace.append(di.項目定義リスト[d_row][2][0])
#
#                     writer.writerow(['項目', di.項目定義リスト[d_row][0][0], '', di.項目定義リスト[d_row][1][0], '○', '', di.項目定義リスト[d_row][2][0], di.項目定義リスト[d_row][7][0], '', '', di.項目定義リスト[d_row][6][0], di.項目定義リスト[d_row][6][1], di.項目定義リスト[d_row][6][2],  di.項目定義リスト[d_row][6][3], '', '', '', '', '', '', ])
#                     writer.writerow(['', '', '', '', '', '', '*', '不明', '不明', '', '', '', '', '', '', '条件指定', '', '', '', '', ])
#                 else:
#                     writer.writerow(['', '', '', '', '○', '子項目', di.項目定義リスト[d_row][2][0], di.項目定義リスト[d_row][7][0], '', '', di.項目定義リスト[d_row][6][0], di.項目定義リスト[d_row][6][1], di.項目定義リスト[d_row][6][2],  di.項目定義リスト[d_row][6][3], '', '', '', '', '', '', ])
#                     # 次の要素が子項目でない場合
#                     if di.項目定義リスト[d_row + 1][8][0] != '〇':
#                         # 次の要素が子項目でない場合、不明を追加してフラグOFF
#                         writer.writerow(['', '', '', '', '', '', '*', '不明', '不明', '', '', '', '', '', '', '条件指定', '', '', '', '', ])
#                         ループフラグ = 0
#
# #Makingでこのタイプはないか
#             elif di.項目定義リスト[d_row][0][0] == 'Making' and di.項目定義リスト[d_row][1][0] == '自由回答':
#
#                 if ループフラグ == 0:
#
#                     MakingPlace.append(di.項目定義リスト[d_row][2][0])
#
#                     writer.writerow(['項目', di.項目定義リスト[d_row][0][0], '', di.項目定義リスト[d_row][1][0], '○', '', di.項目定義リスト[d_row][2][0], di.項目定義リスト[d_row][7][0], '', '', '', '', '', '', '', '', '', '', '', '', ])
#                     writer.writerow(['', '', '', '', '', '', '*', '不明', '不明', '', '', '', '', '', '', '条件指定', '', '', '', '', ])
#                 else:
#                     writer.writerow(['', '', '', '', '○', '子項目', di.項目定義リスト[d_row][2][0], di.項目定義リスト[d_row][7][0], '', '', '', '', '', '', '', '', '', '', '', '', ])
#                     # 次の要素が子項目でない場合
#                     if di.項目定義リスト[d_row + 1][8][0] != '〇':
#                         # 次の要素が子項目でない場合、不明を追加してフラグOFF
#                         writer.writerow(['', '', '', '', '', '', '*', '不明', '不明', '', '', '', '', '', '', '条件指定', '', '', '', '', ])
#                         ループフラグ = 0
#
#             elif di.項目定義リスト[d_row][0][0] == 'Survey' and di.項目定義リスト[d_row][1][0] in ('ﾙｰﾌﾟ(SA)','ﾙｰﾌﾟ(MA)'):
#
#                 # 次の要素が子項目でない場合エラー
#                 if di.項目定義リスト[d_row + 1][8][0] != '〇':
#                     print(di.項目定義リスト[d_row + 1])
#                     logger.error("ループ項目に対する子項目が存在しません。 : %s行目"  % (d_row + 1))
#                     sys.exit()
#
#                 writer.writerow(['項目', di.項目定義リスト[d_row][0][0], '', di.項目定義リスト[d_row][1][0], '○', '', di.項目定義リスト[d_row][2][0], di.項目定義リスト[d_row][7][0], '', '', '', '', '', '', '', '', '', '', '', '', ])
#                 ループフラグ = 1
#
# #ループでこのタイプは出会ってない
#             elif di.項目定義リスト[d_row][0][0] == 'Survey' and di.項目定義リスト[d_row][1][0] in ('ﾙｰﾌﾟ(数値)','ﾙｰﾌﾟ(文字列)'):
#
#                 # 次の要素が子項目でない場合エラー
#                 if di.項目定義リスト[d_row + 1][8][0] != '〇':
#                     logger.error("ループ項目に対する子項目が存在しません。 : %s行目"  % (d_row + 1))
#                     sys.exit()
#
#                 writer.writerow(['項目', di.項目定義リスト[d_row][0][0], '', di.項目定義リスト[d_row][1][0], '○', '', di.項目定義リスト[d_row][2][0], di.項目定義リスト[d_row][7][0], '', '', di.項目定義リスト[d_row][6][0], di.項目定義リスト[d_row][6][1], di.項目定義リスト[d_row][6][2],  di.項目定義リスト[d_row][6][3], '', '', '', '', '', '', ])
#                 ループフラグ = 1
#
# #ループでこのタイプは出会ってない
#             elif di.項目定義リスト[d_row][0][0] == 'Survey' and di.項目定義リスト[d_row][1][0] == 'ﾙｰﾌﾟ(自由回答)':
#
#                 # 次の要素が子項目でない場合エラー
#                 if di.項目定義リスト[d_row + 1][8][0] != '〇':
#                     logger.error("ループ項目に対する子項目が存在しません。 : %s行目"  % (d_row + 1))
#                     sys.exit()
#
#                 writer.writerow(['項目', di.項目定義リスト[d_row][0][0], '', di.項目定義リスト[d_row][1][0], '○', '', di.項目定義リスト[d_row][2][0], di.項目定義リスト[d_row][7][0], '', '', '', '', '', '', '', '', '', '', '', '', ])
#                 ループフラグ = 1
#
#             elif di.項目定義リスト[d_row][0][0] == 'Making' and di.項目定義リスト[d_row][1][0] in ('ﾙｰﾌﾟ(SA)','ﾙｰﾌﾟ(MA)'):
#
#                 # 次の要素が子項目でない場合エラー
#                 if di.項目定義リスト[d_row + 1][8][0] != '〇':
#                     print(di.項目定義リスト[d_row + 1])
#                     logger.error("ループ項目に対する子項目が存在しません。 : %s行目"  % (d_row + 1))
#                     sys.exit()
#
#                 MakingPlace.append(di.項目定義リスト[d_row][2][0])
#
#                 writer.writerow(['項目', di.項目定義リスト[d_row][0][0], '', di.項目定義リスト[d_row][1][0], '○', '', di.項目定義リスト[d_row][2][0], di.項目定義リスト[d_row][7][0], '', '', '', '', '', '', '', '', '', '', '', '', ])
#                 ループフラグ = 1
#
# #Makingでこのタイプはないか
#             elif di.項目定義リスト[d_row][0][0] == 'Making' and di.項目定義リスト[d_row][1][0] in ('ﾙｰﾌﾟ(数値)','ﾙｰﾌﾟ(文字列)'):
#
#                 # 次の要素が子項目でない場合エラー
#                 if di.項目定義リスト[d_row + 1][8][0] != '〇':
#                     logger.error("ループ項目に対する子項目が存在しません。 : %s行目"  % (d_row + 1))
#                     sys.exit()
#
#                 MakingPlace.append(di.項目定義リスト[d_row][2][0])
#
#                 writer.writerow(['項目', di.項目定義リスト[d_row][0][0], '', di.項目定義リスト[d_row][1][0], '○', '', di.項目定義リスト[d_row][2][0], di.項目定義リスト[d_row][7][0], '', '', di.項目定義リスト[d_row][6][0], di.項目定義リスト[d_row][6][1], di.項目定義リスト[d_row][6][2],  di.項目定義リスト[d_row][6][3], '', '', '', '', '', '', ])
#                 ループフラグ = 1
#
# #Makingでこのタイプはないか
#             elif di.項目定義リスト[d_row][0][0] == 'Making' and di.項目定義リスト[d_row][1][0] == 'ﾙｰﾌﾟ(自由回答)':
#
#                 # 次の要素が子項目でない場合エラー
#                 if di.項目定義リスト[d_row + 1][8][0] != '〇':
#                     logger.error("ループ項目に対する子項目が存在しません。 : %s行目"  % (d_row + 1))
#                     sys.exit()
#
#                 MakingPlace.append(di.項目定義リスト[d_row][2][0])
#
#                 writer.writerow(['項目', di.項目定義リスト[d_row][0][0], '', di.項目定義リスト[d_row][1][0], '○', '', di.項目定義リスト[d_row][2][0], di.項目定義リスト[d_row][7][0], '', '', '', '', '', '', '', '', '', '', '', '', ])
#                 ループフラグ = 1
#
#             else:
#                 logger.error("未対応の形式、項目タイプが存在します。形式：%s、項目タイプ：%s"  % (di.項目定義リスト[d_row][0][0], di.項目定義リスト[d_row][1][0]))
#                 sys.exit()
#
#         # 定義ファイルフッターを設定
#         writer.writerow(['終了', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ])
#
#     logger.info("項目定義ファイル作成終了")
#
#     if len(MakingPlace) > 0:
#         logger.info("Making項目が存在します。カテゴライズ条件は手動で設定してください。")
#         logger.info("%s" % (MakingPlace))

def generate_definition_xlsx(file):
    logger.info("項目定義ファイル作成開始: %s" % file)

    ITEM = []

    # 定義ファイルヘッダーを設定する
    ITEM.append(['', '', '項目', '項目', '客先', 'ループ', '', '', '選択肢', '選択肢', '桁数', '最小値', '最大値', '小数点', '', 'カテゴライズ', '特別条件', '集計ベース', '', '調査票'])
    ITEM.append(['行区分', '形式', 'コード', 'タイプ', '表示', '子項目', 'ID', 'ラベル', 'ﾀｲﾌﾟ', 'ｳｴｲﾄ', '', '', '', '以下桁', '統計量', '条件', '', '集計ベースタイプ', '条件', 'ラベル'])

    MakingPlace = []
    ループフラグ = 0

    # 定義リストの要素分を繰り返す
    for d_row in range(0, len(di.項目定義リスト)):

        # 定義に関係ない項目はスキップ
        if di.項目定義リスト[d_row][0][0] in ('Impdata','Joken','Temp','Merge'):
            continue

        # 形式、項目タイプ毎で項目定義を設定する
        if di.項目定義リスト[d_row][0][0] == 'Survey' and di.項目定義リスト[d_row][1][0] in ('SA','MA'):

            if ループフラグ == 0:
                ITEM.append(['項目', di.項目定義リスト[d_row][0][0], '', di.項目定義リスト[d_row][1][0], '○', '', di.項目定義リスト[d_row][2][0], di.項目定義リスト[d_row][7][0], '', '', '', '', '', '', '', '', '', '', '', '', ])
                # 選択肢リストの要素分を繰り返す
                for i in range(1, len(di.項目定義リスト[d_row][6][0])):
                    ITEM.append(['', '', '', '', '', '', i, di.項目定義リスト[d_row][6][0][i][0], di.項目定義リスト[d_row][6][0][i][1], '', '', '', '', '', '', '', '', '', '', '', ])
            else:
                ITEM.append(['', '', '', '', '○', '子項目', di.項目定義リスト[d_row][2][0], di.項目定義リスト[d_row][7][0], '', '', '', '', '', '', '', '', '', '', '', '', ])
                # 次の要素が子項目でない場合
                if di.項目定義リスト[d_row + 1][0][0] in ('Impdata','Temp'):
                    # 選択肢リストの要素分を繰り返す
                    for i in range(1, len(di.項目定義リスト[d_row][6][0])):
                        ITEM.append(['', '', '', '', '', '', i, di.項目定義リスト[d_row][6][0][i][0], di.項目定義リスト[d_row][6][0][i][1], '', '', '', '', '', '', '', '', '', '', '', ])
                    ループフラグ = 0
                else:
                    if di.項目定義リスト[d_row + 1][8][0] != '〇':
                        # 選択肢リストの要素分を繰り返す
                        for i in range(1, len(di.項目定義リスト[d_row][6][0])):
                            ITEM.append(['', '', '', '', '', '', i, di.項目定義リスト[d_row][6][0][i][0], di.項目定義リスト[d_row][6][0][i][1], '', '', '', '', '', '', '', '', '', '', '', ])
                        ループフラグ = 0

        elif di.項目定義リスト[d_row][0][0] == 'Survey' and di.項目定義リスト[d_row][1][0] in ('数値','文字列'):

            if ループフラグ == 0:
                ITEM.append(['項目', di.項目定義リスト[d_row][0][0], '', di.項目定義リスト[d_row][1][0], '○', '', di.項目定義リスト[d_row][2][0], di.項目定義リスト[d_row][7][0], '', '', di.項目定義リスト[d_row][6][0], di.項目定義リスト[d_row][6][1], di.項目定義リスト[d_row][6][2],  di.項目定義リスト[d_row][6][3], '', '', '', '', '', '', ])
                ITEM.append(['', '', '', '', '', '', '*', '不明', '不明', '', '', '', '', '', '', '', '', '', '', '', ])
            else:
                ITEM.append(['', '', '', '', '○', '子項目', di.項目定義リスト[d_row][2][0], di.項目定義リスト[d_row][7][0], '', '', di.項目定義リスト[d_row][6][0], di.項目定義リスト[d_row][6][1], di.項目定義リスト[d_row][6][2],  di.項目定義リスト[d_row][6][3], '', '', '', '', '', '', ])
                # 次の要素が子項目でない場合
                if di.項目定義リスト[d_row + 1][0][0] in ('Impdata','Temp'):
                    # 次の要素が子項目でない場合、不明を追加してフラグOFF
                    ITEM.append(['', '', '', '', '', '', '*', '不明', '不明', '', '', '', '', '', '', '', '', '', '', '', ])
                    ループフラグ = 0
                else:
                    if di.項目定義リスト[d_row + 1][8][0] != '〇':
                        # 次の要素が子項目でない場合、不明を追加してフラグOFF
                        ITEM.append(['', '', '', '', '', '', '*', '不明', '不明', '', '', '', '', '', '', '', '', '', '', '', ])
                        ループフラグ = 0

        elif di.項目定義リスト[d_row][0][0] == 'Survey' and di.項目定義リスト[d_row][1][0] == '自由回答':

            if ループフラグ == 0:
                ITEM.append(['項目', di.項目定義リスト[d_row][0][0], '', di.項目定義リスト[d_row][1][0], '○', '', di.項目定義リスト[d_row][2][0], di.項目定義リスト[d_row][7][0], '', '', '', '', '', '', '', '', '', '', '', '', ])
                ITEM.append(['', '', '', '', '', '', '*', '不明', '不明', '', '', '', '', '', '', '', '', '', '', '', ])
            else:
                ITEM.append(['', '', '', '', '○', '子項目', di.項目定義リスト[d_row][2][0], di.項目定義リスト[d_row][7][0], '', '', '', '', '', '', '', '', '', '', '', '', ])
                # 次の要素が子項目でない場合
                if di.項目定義リスト[d_row + 1][0][0] in ('Impdata','Temp'):
                    # 次の要素が子項目でない場合、不明を追加してフラグOFF
                    ITEM.append(['', '', '', '', '', '', '*', '不明', '不明', '', '', '', '', '', '', '', '', '', '', '', ])
                    ループフラグ = 0
                else:
                    if di.項目定義リスト[d_row + 1][8][0] != '〇':
                        # 次の要素が子項目でない場合、不明を追加してフラグOFF
                        ITEM.append(['', '', '', '', '', '', '*', '不明', '不明', '', '', '', '', '', '', '', '', '', '', '', ])
                        ループフラグ = 0

        elif di.項目定義リスト[d_row][0][0] == 'Making' and di.項目定義リスト[d_row][1][0] in ('SA','MA'):

            if ループフラグ == 0:

                MakingPlace.append(di.項目定義リスト[d_row][2][0])

                ITEM.append(['項目', di.項目定義リスト[d_row][0][0], '', di.項目定義リスト[d_row][1][0], '○', '', di.項目定義リスト[d_row][2][0], di.項目定義リスト[d_row][7][0], '', '', '', '', '', '', '', '', '', '', '', '', ])
                # 選択肢リストの要素分を繰り返す
                for i in range(1, len(di.項目定義リスト[d_row][6][0])):
                    ITEM.append(['', '', '', '', '', '', i, di.項目定義リスト[d_row][6][0][i][0], di.項目定義リスト[d_row][6][0][i][1], '', '', '', '', '', '', '条件指定', '', '', '', '', ])
            else:
                ITEM.append(['', '', '', '', '○', '子項目', di.項目定義リスト[d_row][2][0], di.項目定義リスト[d_row][7][0], '', '', '', '', '', '', '', '', '', '', '', '', ])
                # 次の要素が子項目でない場合
                if di.項目定義リスト[d_row + 1][0][0] in ('Impdata','Temp'):
                    # 選択肢リストの要素分を繰り返す
                    for i in range(1, len(di.項目定義リスト[d_row][6][0])):
                        ITEM.append(['', '', '', '', '', '', i, di.項目定義リスト[d_row][6][0][i][0], di.項目定義リスト[d_row][6][0][i][1], '', '', '', '', '', '', '', '', '', '', '', ])
                    ループフラグ = 0
                else:
                    if di.項目定義リスト[d_row + 1][8][0] != '〇':
                        # 選択肢リストの要素分を繰り返す
                        for i in range(1, len(di.項目定義リスト[d_row][6][0])):
                            ITEM.append(['', '', '', '', '', '', i, di.項目定義リスト[d_row][6][0][i][0], di.項目定義リスト[d_row][6][0][i][1], '', '', '', '', '', '', '', '', '', '', '', ])
                        ループフラグ = 0

#Makingでこのタイプはないか
        elif di.項目定義リスト[d_row][0][0] == 'Making' and di.項目定義リスト[d_row][1][0] in ('数値','文字列'):

            if ループフラグ == 0:

                MakingPlace.append(di.項目定義リスト[d_row][2][0])

                ITEM.append(['項目', di.項目定義リスト[d_row][0][0], '', di.項目定義リスト[d_row][1][0], '○', '', di.項目定義リスト[d_row][2][0], di.項目定義リスト[d_row][7][0], '', '', di.項目定義リスト[d_row][6][0], di.項目定義リスト[d_row][6][1], di.項目定義リスト[d_row][6][2],  di.項目定義リスト[d_row][6][3], '', '', '', '', '', '', ])
                ITEM.append(['', '', '', '', '', '', '*', '不明', '不明', '', '', '', '', '', '', '条件指定', '', '', '', '', ])
            else:
                ITEM.append(['', '', '', '', '○', '子項目', di.項目定義リスト[d_row][2][0], di.項目定義リスト[d_row][7][0], '', '', di.項目定義リスト[d_row][6][0], di.項目定義リスト[d_row][6][1], di.項目定義リスト[d_row][6][2],  di.項目定義リスト[d_row][6][3], '', '', '', '', '', '', ])
                # 次の要素が子項目でない場合
                if di.項目定義リスト[d_row + 1][0][0] in ('Impdata','Temp'):
                    # 次の要素が子項目でない場合、不明を追加してフラグOFF
                    ITEM.append(['', '', '', '', '', '', '*', '不明', '不明', '', '', '', '', '', '', '', '', '', '', '', ])
                    ループフラグ = 0
                else:
                    if di.項目定義リスト[d_row + 1][8][0] != '〇':
                        # 次の要素が子項目でない場合、不明を追加してフラグOFF
                        ITEM.append(['', '', '', '', '', '', '*', '不明', '不明', '', '', '', '', '', '', '', '', '', '', '', ])
                        ループフラグ = 0

#Makingでこのタイプはないか
        elif di.項目定義リスト[d_row][0][0] == 'Making' and di.項目定義リスト[d_row][1][0] == '自由回答':

            if ループフラグ == 0:

                MakingPlace.append(di.項目定義リスト[d_row][2][0])

                ITEM.append(['項目', di.項目定義リスト[d_row][0][0], '', di.項目定義リスト[d_row][1][0], '○', '', di.項目定義リスト[d_row][2][0], di.項目定義リスト[d_row][7][0], '', '', '', '', '', '', '', '', '', '', '', '', ])
                ITEM.append(['', '', '', '', '', '', '*', '不明', '不明', '', '', '', '', '', '', '条件指定', '', '', '', '', ])
            else:
                ITEM.append(['', '', '', '', '○', '子項目', di.項目定義リスト[d_row][2][0], di.項目定義リスト[d_row][7][0], '', '', '', '', '', '', '', '', '', '', '', '', ])
                # 次の要素が子項目でない場合
                if di.項目定義リスト[d_row + 1][0][0] in ('Impdata','Temp'):
                    # 次の要素が子項目でない場合、不明を追加してフラグOFF
                    ITEM.append(['', '', '', '', '', '', '*', '不明', '不明', '', '', '', '', '', '', '', '', '', '', '', ])
                    ループフラグ = 0
                else:
                    if di.項目定義リスト[d_row + 1][8][0] != '〇':
                        # 次の要素が子項目でない場合、不明を追加してフラグOFF
                        ITEM.append(['', '', '', '', '', '', '*', '不明', '不明', '', '', '', '', '', '', '', '', '', '', '', ])
                        ループフラグ = 0

        elif di.項目定義リスト[d_row][0][0] == 'Survey' and di.項目定義リスト[d_row][1][0] in ('ﾙｰﾌﾟ(SA)','ﾙｰﾌﾟ(MA)'):

            # 次の要素が子項目でない場合エラー
            if di.項目定義リスト[d_row + 1][8][0] != '〇':
                logger.error("ループ項目に対する子項目が存在しません。 : %s行目"  % (d_row + 1))
                sys.exit()

            ITEM.append(['項目', di.項目定義リスト[d_row][0][0], '', di.項目定義リスト[d_row][1][0], '○', '', di.項目定義リスト[d_row][2][0], di.項目定義リスト[d_row][7][0], '', '', '', '', '', '', '', '', '', '', '', '', ])
            ループフラグ = 1

#ループでこのタイプは出会ってない
        elif di.項目定義リスト[d_row][0][0] == 'Survey' and di.項目定義リスト[d_row][1][0] in ('ﾙｰﾌﾟ(数値)','ﾙｰﾌﾟ(文字列)'):

            # 次の要素が子項目でない場合エラー
            if di.項目定義リスト[d_row + 1][8][0] != '〇':
                logger.error("ループ項目に対する子項目が存在しません。 : %s行目"  % (d_row + 1))
                sys.exit()

            ITEM.append(['項目', di.項目定義リスト[d_row][0][0], '', di.項目定義リスト[d_row][1][0], '○', '', di.項目定義リスト[d_row][2][0], di.項目定義リスト[d_row][7][0], '', '', di.項目定義リスト[d_row][6][0], di.項目定義リスト[d_row][6][1], di.項目定義リスト[d_row][6][2],  di.項目定義リスト[d_row][6][3], '', '', '', '', '', '', ])
            ループフラグ = 1

#ループでこのタイプは出会ってない
        elif di.項目定義リスト[d_row][0][0] == 'Survey' and di.項目定義リスト[d_row][1][0] == 'ﾙｰﾌﾟ(自由回答)':

            # 次の要素が子項目でない場合エラー
            if di.項目定義リスト[d_row + 1][8][0] != '〇':
                logger.error("ループ項目に対する子項目が存在しません。 : %s行目"  % (d_row + 1))
                sys.exit()

            ITEM.append(['項目', di.項目定義リスト[d_row][0][0], '', di.項目定義リスト[d_row][1][0], '○', '', di.項目定義リスト[d_row][2][0], di.項目定義リスト[d_row][7][0], '', '', '', '', '', '', '', '', '', '', '', '', ])
            ループフラグ = 1

        elif di.項目定義リスト[d_row][0][0] == 'Making' and di.項目定義リスト[d_row][1][0] in ('ﾙｰﾌﾟ(SA)','ﾙｰﾌﾟ(MA)'):

            # 次の要素が子項目でない場合エラー
            if di.項目定義リスト[d_row + 1][8][0] != '〇':
                print(di.項目定義リスト[d_row + 1])
                logger.error("ループ項目に対する子項目が存在しません。 : %s行目"  % (d_row + 1))
                sys.exit()

            MakingPlace.append(di.項目定義リスト[d_row][2][0])

            ITEM.append(['項目', di.項目定義リスト[d_row][0][0], '', di.項目定義リスト[d_row][1][0], '○', '', di.項目定義リスト[d_row][2][0], di.項目定義リスト[d_row][7][0], '', '', '', '', '', '', '', '', '', '', '', '', ])
            ループフラグ = 1

#Makingでこのタイプはないか
        elif di.項目定義リスト[d_row][0][0] == 'Making' and di.項目定義リスト[d_row][1][0] in ('ﾙｰﾌﾟ(数値)','ﾙｰﾌﾟ(文字列)'):

            # 次の要素が子項目でない場合エラー
            if di.項目定義リスト[d_row + 1][8][0] != '〇':
                logger.error("ループ項目に対する子項目が存在しません。 : %s行目"  % (d_row + 1))
                sys.exit()

            MakingPlace.append(di.項目定義リスト[d_row][2][0])

            ITEM.append(['項目', di.項目定義リスト[d_row][0][0], '', di.項目定義リスト[d_row][1][0], '○', '', di.項目定義リスト[d_row][2][0], di.項目定義リスト[d_row][7][0], '', '', di.項目定義リスト[d_row][6][0], di.項目定義リスト[d_row][6][1], di.項目定義リスト[d_row][6][2],  di.項目定義リスト[d_row][6][3], '', '', '', '', '', '', ])
            ループフラグ = 1

#Makingでこのタイプはないか
        elif di.項目定義リスト[d_row][0][0] == 'Making' and di.項目定義リスト[d_row][1][0] == 'ﾙｰﾌﾟ(自由回答)':

            # 次の要素が子項目でない場合エラー
            if di.項目定義リスト[d_row + 1][8][0] != '〇':
                logger.error("ループ項目に対する子項目が存在しません。 : %s行目"  % (d_row + 1))
                sys.exit()

            MakingPlace.append(di.項目定義リスト[d_row][2][0])

            ITEM.append(['項目', di.項目定義リスト[d_row][0][0], '', di.項目定義リスト[d_row][1][0], '○', '', di.項目定義リスト[d_row][2][0], di.項目定義リスト[d_row][7][0], '', '', '', '', '', '', '', '', '', '', '', '', ])
            ループフラグ = 1

        else:
            logger.error("未対応の形式、項目タイプが存在します。形式：%s、項目タイプ：%s"  % (di.項目定義リスト[d_row][0][0], di.項目定義リスト[d_row][1][0]))
            sys.exit()

    # 定義ファイルフッターを設定
    ITEM.append(['終了', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ])

    # エクセルに項目定義を書き込み
    df = pandas.DataFrame(ITEM)
    df.to_excel(file, index=False, header=False)

    logger.info("項目定義ファイル作成終了")

    if len(MakingPlace) > 0:
        logger.info("Making項目が存在します。カテゴライズ条件は手動で設定してください。")
        logger.info("%s" % (MakingPlace))

def main():
    logger.info("Start")

    # 定義ファイル作成
    generate_definition_xlsx(dest + "/lyche_definition.xlsx")

    # 内部用の空テーブル作成
    with closing(sqlite3.connect(':memory:')) as conn:
        init_table(conn)

        # データ取り込み
        UnknownList.append(['選択肢名', '取得値'])
        imp_data(conn)

        if len(UnknownList) > 1:
            logger.error('SA選択肢に存在しない取得値を確認してください。「UnknownList.csv」')
            # csvファイルをオープンする
            with open('UnknownList.csv', 'w', encoding='utf_8_sig', newline="") as fdw:
                writer = csv.writer(fdw, lineterminator='\r\n')
                for i in range(0, len(UnknownList)):
                    writer.writerow(UnknownList[i])
            sys.exit()

# デバック用
#          # 統合テーブル全出力
#         c = conn.cursor()
#         sql_debug = "select * from 統合"
#         c.execute(sql_debug)
#         rs = c.fetchall()
#         for rb in rs:
#             print(rb)

        # インポートデータファイル作成
#        generate_import_csv(conn, dest + "/lyche_importdata.csv")
        generate_import_xlsx(conn, dest + "/lyche_importdata.xlsx")

    logger.info('Finish.')

if __name__ == '__main__':
    main()
